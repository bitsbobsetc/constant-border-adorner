﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace ConstantBorderAdorner
{
    public class BorderAdorner<T> : Adorner where T : Control
    {
        private double x_offset = 0;
        private double y_offset = 0;

        private ScaleTransform _scaleTransform;

        private readonly FrameworkElement _adornedElement;

        public BorderAdorner(
            FrameworkElement adornedElement)
            : base(adornedElement)
        {
            _adornedElement = adornedElement;
        }

        protected override int VisualChildrenCount
        {
            get { return 1; }
        }

        protected override Visual GetVisualChild(int index)
        {
            if (index != 0)
            {
                throw new ArgumentOutOfRangeException();
            }

            return Child;
        }

        public virtual void SetChild(T child)
        {
            if (Child != null)
            {
                RemoveVisualChild(Child);
            }

            Child = child;

            if (Child != null)
            {
                AddVisualChild(Child);
            }
        }

        protected T Child { get; set; }

        protected override Size MeasureOverride(Size constraint)
        {
            Child.Measure(constraint);

            return Child.DesiredSize;
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            ArrangeChild(finalSize);

            if (_scaleTransform != null)
            {
                if ((Math.Abs(_scaleTransform.ScaleX) > 1.0) || (Math.Abs(_scaleTransform.ScaleY) > 1.0))
                {
                    Child.Arrange(new Rect(new Point(x_offset, y_offset), finalSize));
                }
            }
            else
            {
                Child.Arrange(new Rect(new Point(x_offset, y_offset), finalSize));
            }

            return new Size(Child.ActualWidth, Child.ActualHeight);
        }

        protected virtual void ArrangeChild(Size finalSize)
        {
            Child.Arrange(new Rect(new Point(0, 0), finalSize));
        }

        public override GeneralTransform GetDesiredTransform(GeneralTransform transform)
        {
            if (_scaleTransform != null)
            {
                Child.RenderTransform = new ScaleTransform(1 / _scaleTransform.ScaleX, 1 / _scaleTransform.ScaleY);
                Child.RenderTransformOrigin = new Point(0, 0);

                Child.Width = _adornedElement.ActualWidth * _scaleTransform.ScaleX;
                Child.Height = _adornedElement.ActualHeight * _scaleTransform.ScaleY;

            }

            return base.GetDesiredTransform(transform);
        }

        public void NotifyScaleChanged(ScaleTransform scaleTransform)
        {
            _scaleTransform = scaleTransform;
        }

        public void NotifyPositionChanged()
        {
            InvalidateVisual();
        }
    }
}
