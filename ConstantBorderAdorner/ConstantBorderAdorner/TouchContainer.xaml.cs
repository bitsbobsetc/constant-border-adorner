﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ConstantBorderAdorner
{
    /// <summary>
    /// Interaction logic for TouchContainer.xaml
    /// </summary>
    public partial class TouchContainer : UserControl
    {
        private readonly MatrixTransform _transform;
        private readonly ScaleTransform _scale;

        private AdornerLayer _adornerLayer;

        public TouchContainer()
        {
            InitializeComponent();

            _scale = new ScaleTransform(1, 1);
            _transform = new MatrixTransform();

            this.RenderTransform = _transform;
            this.LayoutTransform = _scale;

            Loaded += OnLoaded;
        }

        public Action<ScaleTransform> NotifyScaleChanged;

        public Action NotifyPositionChanged;

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            _adornerLayer = AdornerLayer.GetAdornerLayer(this);

            var adorner = new BorderAdorner<ContainerBorder>(this);

            var containerBorder = new ContainerBorder
            {
                Width = ActualWidth,
                Height = ActualHeight
            };

            NotifyScaleChanged += adorner.NotifyScaleChanged;
            NotifyPositionChanged += adorner.NotifyPositionChanged;

            adorner.SetChild(containerBorder);
            _adornerLayer.Add(adorner);
        }

        protected override void OnManipulationStarting(ManipulationStartingEventArgs e)
        {
            e.ManipulationContainer = Parent as FrameworkElement;
            e.Handled = true;
        }

        protected override void OnManipulationDelta(ManipulationDeltaEventArgs e)
        {
            var delta = e.DeltaManipulation;
            var center = e.ManipulationOrigin;

            _scale.ScaleX *= delta.Scale.X;
            _scale.ScaleY *= delta.Scale.Y;

            var xform = RenderTransform as MatrixTransform;

            if (xform != null)
            {
                Matrix matrix = xform.Matrix;

                matrix.RotateAt(delta.Rotation, center.X, center.Y);
                matrix.Translate(delta.Translation.X, delta.Translation.Y);

                if (NotifyScaleChanged != null)
                    NotifyScaleChanged(_scale);

                xform.Matrix = matrix;
            }

            UpdateLayout();

            if (NotifyPositionChanged != null)
                NotifyPositionChanged();

            e.Handled = true;
        }
    }
}
